import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shared/services/auth.service';
import { HttpClient } from '@angular/common/http';
import { SessionService } from '../shared/services/session.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})


export class UsersComponent implements OnInit {

	user: any = [];
	dtOptions: DataTables.Settings = {};
	data: any = [];

  	constructor(private authService: AuthService,  private sessionService: SessionService, private http: HttpClient) { }

  	ngOnInit() {
  		this.user =  this.sessionService.getItem('userClaim');
  		this.http.get('http://localhost/binlab_backend/api/users/'+this.user.lab_id+'#'+this.user.userId).subscribe(success => {
      		this.data = success;
      		console.log(success);
      		this.dtOptions = {
      			pagingType: 'full_numbers',
      			pageLength: 5,
      			processing: true
    		};
    	});
  	}
}
